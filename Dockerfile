FROM alpine:3.15.4

RUN apk add python3 py3-pip nodejs zsh npm tzdata

ADD . /app

WORKDIR /app

ENV DJANGO_SETTINGS_MODULE=backend.settings

RUN cd frontend && npm install && npm run build && cd .. && \
    pip install -r requirements.txt && \
    python3 ./manage.py migrate && \
    python3 ./manage.py generate_fake_data 100

CMD ["python3", "/app/manage.py", "runserver", "0.0.0.0:8000"]