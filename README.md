# Quick start
Build docker image
> docker build -t test_shipments .

Start docker container

> docker run -p 8000:8000 test_shipments

Open next url in your browser

> http://127.0.0.1:8000

# Backend
## Build
Install required packages
> pip install -r requirements.txt

Create DB tables
> ./manage.py migrate

Create superuser
> ./manage.py createsuperuser --email admin@mail.com --username admin

Generate fake records, number at the end of the command is the number of records to create
>  ./manage.py generate_fake_data 100

## Test
> manage.py test

#Frontend
## Build
You need nodejs >= 14

> cd frontend
>
> npm install
>
> npm run lint
> 
> npm run dev

