from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from backend.shipments import views

router = routers.DefaultRouter()
router.register(r'shipments', views.ShipmentViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('rest/', include(router.urls)),
    path('rest-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
