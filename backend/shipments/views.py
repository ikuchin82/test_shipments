from django.shortcuts import render

from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions

from backend.shipments.models import Shipment
from backend.shipments.serializers import UserSerializer, GroupSerializer, ShipmentSerializer


class ShipmentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Shipment to be viewed or edited.
    """
    queryset = Shipment.objects.all()
    serializer_class = ShipmentSerializer
    permission_classes = []

    def get_queryset(self):
        sort_by = self.request.query_params.get('sortBy')
        descending = "-" if self.request.query_params.get('descending') == "true" else ""
        search = self.request.query_params.get('filter')

        queryset = Shipment.objects.all()

        if search:
            queryset = queryset.filter(description__contains=search)

        if sort_by:
            queryset = queryset.order_by(f'{descending}{sort_by}')

        return queryset


def index(request):
    return render(request, 'index.html')
