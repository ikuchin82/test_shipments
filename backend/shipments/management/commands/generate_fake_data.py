from django.core.management.base import BaseCommand, CommandError
from backend.shipments.models import Shipment


class Command(BaseCommand):
    help = 'Create fake data'

    def add_arguments(self, parser):
        parser.add_argument('number_of_records_to_create', type=int)

    def handle(self, *args, **options):
        for i, x in enumerate(range(options['number_of_records_to_create']), start=1):
            Shipment(
                order_placed="2020-03-01T01:01:01Z",
                order_shipped="2020-03-02T01:01:01Z",
                order_delivered="2020-03-03T01:01:01Z",
                description=f"banana_{i}",
                quantity=i,
            ).save()
