from django.db import models


class Shipment(models.Model):
    order_placed = models.DateTimeField()
    order_shipped = models.DateTimeField(null=True, blank=True, default=None)
    order_delivered = models.DateTimeField(null=True, blank=True, default=None)
    description = models.TextField()
    quantity = models.IntegerField()
