import json
from datetime import datetime

from django.test import TestCase, override_settings
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from backend.shipments.models import Shipment


class TestRest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_data = [
            {
                "order_placed": "2020-01-01T01:01:01Z",
                "order_shipped": "2020-01-02T01:01:01Z",
                "order_delivered": "2020-01-03T01:01:01Z",
                "description": "banana_1",
                "quantity": 3,
            },
            {
                "order_placed": "2020-02-01T01:01:01Z",
                "order_shipped": "2020-02-02T01:01:01Z",
                "order_delivered": "2020-02-03T01:01:01Z",
                "description": "banana_2",
                "quantity": 2,
            },
            {
                "order_placed": "2020-03-01T01:01:01Z",
                "order_shipped": "2020-03-02T01:01:01Z",
                "order_delivered": "2020-03-03T01:01:01Z",
                "description": "banana_3",
                "quantity": 1,
            },
        ]

        Shipment.objects.bulk_create(Shipment(**kw) for kw in cls.test_data)

    def test_list(self):
        client = APIClient()
        response = client.get('/rest/shipments/?limit=10')

        self.assertEqual(response.status_code, 200)

        response_json = response.json()

        self.assertEqual(response_json['count'], 3)
        self.assertEqual(response_json['next'], None)
        self.assertEqual(response_json['previous'], None)
        for a, b in zip(self.test_data, response_json['results']):
            self.assertEqual(b, b | a)

    def test_list_sorting(self):
        client = APIClient()
        response = client.get('/rest/shipments/?sortBy=quantity&limit=10')

        self.assertEqual(response.status_code, 200)

        response_json = response.json()

        self.assertEqual(response_json['count'], 3)
        self.assertEqual(response_json['next'], None)
        self.assertEqual(response_json['previous'], None)

        self.assertEqual([3, 2, 1], [x['id'] for x in response_json['results']])

    def test_list_reverse_sorting(self):
        client = APIClient()
        response = client.get('/rest/shipments/?sortBy=quantity&descending=true&limit=10')

        self.assertEqual(response.status_code, 200)

        response_json = response.json()

        self.assertEqual(response_json['count'], 3)
        self.assertEqual(response_json['next'], None)
        self.assertEqual(response_json['previous'], None)

        self.assertEqual([1, 2, 3], [x['id'] for x in response_json['results']])

    def test_list_filtering(self):
        client = APIClient()
        response = client.get('/rest/shipments/?filter=_1&limit=10')

        self.assertEqual(response.status_code, 200)

        response_json = response.json()

        self.assertEqual(response_json['count'], 1)
        self.assertEqual(response_json['next'], None)
        self.assertEqual(response_json['previous'], None)

        self.assertEqual([1], [x['id'] for x in response_json['results']])

    def test_list_pagination_offset_0(self):
        client = APIClient()
        response = client.get('/rest/shipments/?limit=2&offset=0')

        self.assertEqual(response.status_code, 200)

        response_json = response.json()

        self.assertEqual(response_json['count'], 3)
        self.assertEqual(response_json['next'], 'http://testserver/rest/shipments/?limit=2&offset=2')
        self.assertEqual(response_json['previous'], None)

        self.assertEqual([1, 2], [x['id'] for x in response_json['results']])

    def test_list_pagination_offset_2(self):
        client = APIClient()
        response = client.get('/rest/shipments/?limit=2&offset=2')

        self.assertEqual(response.status_code, 200)

        response_json = response.json()

        self.assertEqual(response_json['count'], 3)
        self.assertEqual(response_json['next'], None)
        self.assertEqual(response_json['previous'], 'http://testserver/rest/shipments/?limit=2')

        self.assertEqual([3], [x['id'] for x in response_json['results']])

    def test_add_valid_record(self):
        client = APIClient()
        response = client.post('/rest/shipments/', {
            'order_placed': datetime.now().isoformat(),
            'description': 'banana',
            'quantity': 1,
        }, format='json')

        self.assertEqual(response.status_code, 201)

        response_json = response.json()

        self.assertEqual(response_json['description'], 'banana')
        self.assertEqual(response_json['id'], 4)

    def test_add_invalid_record_missing_field(self):
        client = APIClient()

        response = client.post('/rest/shipments/', {
            'order_placed': datetime.now().isoformat(),
            'description': 'banana',
        }, format='json')

        self.assertEqual(response.status_code, 400)

        response_json = response.json()

        self.assertEqual(response_json, {'quantity': ['This field is required.']}, response_json)

    def test_add_invalid_record_field_has_wrong_type(self):
        client = APIClient()

        response = client.post('/rest/shipments/', {
            'order_placed': datetime.now().isoformat(),
            'description': 'banana',
            'quantity': 'abc'
        }, format='json')

        self.assertEqual(response.status_code, 400)

        response_json = response.json()

        self.assertEqual(response_json, {'quantity': ['A valid integer is required.']}, response_json)
