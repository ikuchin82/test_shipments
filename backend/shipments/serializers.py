from django.contrib.auth.models import User, Group
from backend.shipments.models import Shipment
from rest_framework import serializers


class ShipmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Shipment
        fields = ['id', 'order_placed', 'order_shipped', 'order_delivered', 'description', 'quantity']

        extra_kwargs = {'order_shipped': {'required': False}}


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']
