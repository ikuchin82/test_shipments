import { createRouter, createWebHashHistory } from "vue-router";
import ShipmentsView from "../views/ShipmentsView.vue";
import ShipmentDetailsView from "../views/ShipmentDetailsView.vue";

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      name: "Shipments",
      component: ShipmentsView,
    },
    {
      path: "/new",
      name: "NewShipment",
      component: ShipmentDetailsView,
    },
    {
      path: "/:id(\\d+)",
      name: "ShipmentDetails",
      component: ShipmentDetailsView,
    },
  ],
});

export default router;
